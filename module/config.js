// Namespace SFRPG Configuration Values
export const T20Config = {};

T20Config.statusEffectIcons = [
    { "id": "abalado", "label": "Abalado", "icon": "systems/tormenta20/icons/conditions/abalado.svg" },
    { "id": "agarrado", "label": "Agarrado", "icon": "systems/tormenta20/icons/conditions/agarrado.svg" },
    { "id": "alquebrado", "label": "Alquebrado", "icon": "systems/tormenta20/icons/conditions/alquebrado.svg" },
    { "id": "apavorado", "label": "Apavorado", "icon": "systems/tormenta20/icons/conditions/apavorado.svg" },
    { "id": "atordoado", "label": "Atordoado", "icon": "systems/tormenta20/icons/conditions/atordoado.svg" },
    { "id": "caido", "label": "Caído", "icon": "systems/tormenta20/icons/conditions/caido.svg" },
    { "id": "cego", "label": "Cego", "icon": "systems/tormenta20/icons/conditions/cego.svg" },
    { "id": "confuso", "label": "Confuso", "icon": "systems/tormenta20/icons/conditions/confuso.svg" },
    { "id": "debilitado", "label": "Debilitado", "icon": "systems/tormenta20/icons/conditions/debilitado.svg" },
    { "id": "desprevenido", "label": "Desprevenido", "icon": "systems/tormenta20/icons/conditions/desprevenido.svg" },
    { "id": "doente", "label": "Doente", "icon": "systems/tormenta20/icons/conditions/doente.svg" },
    { "id": "emchamas", "label": "Em Chamas", "icon": "systems/tormenta20/icons/conditions/em-chamas.svg" },
    { "id": "enjoado", "label": "Enjoado", "icon": "systems/tormenta20/icons/conditions/enjoado.svg" },
    { "id": "enredado", "label": "Enredado", "icon": "systems/tormenta20/icons/conditions/enredado.svg" },
    { "id": "envenenado", "label": "Envenenado", "icon": "systems/tormenta20/icons/conditions/envenenado.svg" },
    { "id": "esmorecido", "label": "Esmorecido", "icon": "systems/tormenta20/icons/conditions/esmorecido.svg" },
    { "id": "exausto", "label": "Exausto", "icon": "systems/tormenta20/icons/conditions/exausto.svg" },
    { "id": "fascinado", "label": "Fascinado", "icon": "systems/tormenta20/icons/conditions/fascinado.svg" },
    { "id": "fatigado", "label": "Fatigado", "icon": "systems/tormenta20/icons/conditions/fatigado.svg" },
    { "id": "fraco", "label": "Fraco", "icon": "systems/tormenta20/icons/conditions/fraco.svg" },
    { "id": "frustrado", "label": "Frustrado", "icon": "systems/tormenta20/icons/conditions/frustrado.svg" },
    { "id": "imovel", "label": "Imóvel", "icon": "systems/tormenta20/icons/conditions/imovel.svg" },
    { "id": "inconsciente", "label": "Inconsciente", "icon": "systems/tormenta20/icons/conditions/inconsciente.svg" },
    { "id": "indefeso", "label": "Indefeso", "icon": "systems/tormenta20/icons/conditions/indefeso.svg" },
    { "id": "lento", "label": "Lento", "icon": "systems/tormenta20/icons/conditions/lento.svg" },
    { "id": "morto", "label": "Morto", "icon": "systems/tormenta20/icons/conditions/morto.svg" },
    { "id": "ofuscado", "label": "Ofuscado", "icon": "systems/tormenta20/icons/conditions/ofuscado.svg" },
    { "id": "paralisado", "label": "Paralisado", "icon": "systems/tormenta20/icons/conditions/paralisado.svg" },
    { "id": "pasmo", "label": "Pasmo", "icon": "systems/tormenta20/icons/conditions/pasmo.svg" },
    { "id": "petrificado", "label": "Petrificado", "icon": "systems/tormenta20/icons/conditions/petrificado.svg" },
    { "id": "sangrando", "label": "Sangrando", "icon": "systems/tormenta20/icons/conditions/sangrando.svg" },
    { "id": "sobrecarregado", "label": "Sobrecarregado", "icon": "systems/tormenta20/icons/conditions/sobrecarregado.svg" },
    { "id": "surdo", "label": "Surdo", "icon": "systems/tormenta20/icons/conditions/surdo.svg" },
    { "id": "surpreendido", "label": "Surpreendido", "icon": "systems/tormenta20/icons/conditions/surpreendido.svg" },
    { "id": "vulneravel", "label": "Vulnerável", "icon": "systems/tormenta20/icons/conditions/vulneravel.svg" }
];

T20Config.conditions = {
    "abalado": {
        "tooltip": "<strong>Abalado</strong><br><br>O personagem sofre –2 em testes de perícia. Se ficar abalado novamente, em vez disso fica apavorado. <i>Condição de medo.</i>"
    },
    "agarrado": {
        "tooltip": "<strong>Agarrado</strong><br><br>O personagem fica desprevenido e imóvel, sofre –2 em testes de ataque e só pode atacar com armas leves. Um personagem fazendo um ataque à distância contra um alvo envolvido na manobra agarrar tem 50% de chance de acertar o alvo errado. <i>Condição de paralisia.</i>"
    },
    "alquebrado": {
        "tooltip": "<strong>Alquebrado</strong><br><br> O custo em pontos de mana das habilidades e magias do personagem aumenta em +1. <i>Condição mental.</i>"
    },
    "apavorado": {
        "tooltip": "<strong>Apavorado</strong><br><br>O personagem sofre –5 em testes de perícia e deve fugir da fonte do medo da maneira mais eficiente possível. Se não puder, poderá agir, mas não poderá se aproximar voluntariamente da fonte do medo. <i>Condição de medo.</i>"
    },
    "atordoado": {
        "tooltip": "<strong>Atordoado</strong><br><br> O personagem fica desprevenido e não pode fazer ações. <i>Condição mental.</i>"
    },
    "caido": {
        "tooltip": "<strong>Caído</strong><br><br> Deitado no chão. O personagem sofre –5 em ataques corpo a corpo e seu deslocamento é reduzido a 1,5m. Além disso, sofre –5 de Defesa contra ataques corpo a corpo, mas recebe +5 de Defesa contra ataques à distância."
    },
    "cego": {
        "tooltip": "<strong>Cego</strong><br><br> O personagem fica desprevenido e lento, não pode fazer testes de Percepção para observar e sofre –5 em testes de perícias baseadas em Força ou Destreza. Todos os alvos de seus ataques recebem camuflagem total. <i>Condição de sentidos.</i>"
    },
    "confuso": {
        "tooltip": "<strong>Confuso</strong><br><br> O personagem comporta-se de modo aleatório. Role 1d6 no início de seus turnos: 1) Movimenta-se em uma direção escolhida por uma rolagem de 1d8; 2-3) Não pode fazer ações, exceto reações, e fica balbuciando incoerentemente; 4-5) Usa a arma que estiver empunhando para atacar a criatura mais próxima, ou a si mesmo se estiver sozinho (nesse caso, apenas role o dano); 6) A condição termina e pode agir normalmente. <i>Condição mental.</i>"
    },
    "debilitado": {
        "tooltip": "<strong>Debilitado</strong><br><br> O personagem sofre –5 em testes de atributos físicos (Força, Destreza e Constituição) e de perícias baseadas nesses atributos. Se o personagem ficar debilitado novamente, em vez disso fica inconsciente."
    },
    "desprevenido": {
        "tooltip": "<strong>Desprevenido</strong><br><br> Despreparado para reagir. O personagem sofre –5 na Defesa e em Reflexos. Você fica desprevenido contra inimigos que não possa ver."
    },
    "doente": {
        "tooltip": "<strong>Doente</strong><br><br> Sob efeito de uma doença."
    },
    "em-chamas": {
        "tooltip": "<strong>Em Chamas</strong><br><br> O personagem está pegando fogo. No início de seus turnos, sofre 1d6 pontos de dano de fogo. O personagem pode gastar uma ação padrão para apagar o fogo com as mãos. Imersão em água também apaga as chamas."
    },
    "enjoado": {
        "tooltip": "<strong>Enjoado</strong><br><br> O personagem só pode realizar uma ação padrão ou de movimento (não ambas) por rodada. Ele pode gastar uma ação padrão para fazer uma investida, mas pode avançar no máximo seu deslocamento (e não o dobro)."
    },
    "enredado": {
        "tooltip": "<strong>Enredado</strong><br><br> O personagem fica lento, vulnerável e sofre –2 em testes de ataque. <i>Condição de paralisia.</i>"
    },
    "envenenado": {
        "tooltip": "<strong>Envenenado</strong><br><br> O efeito desta condição varia de acordo com o veneno. Pode ser outra condição (por exemplo, fraco ou enjoado) ou dano recorrente (por exemplo, 1d12 pontos de dano por rodada). A descrição do veneno determina a duração dele (caso nada seja dito, a condição dura pela cena)."
    },
    "esmorecido": {
        "tooltip": "<strong>Esmorecido</strong><br><br> O personagem sofre –5 em testes de atributos mentais (Inteligência, Sabedoria e Carisma) e de perícias baseadas nesses atributos. <i>Condição mental.</i>"
    },
    "exausto": {
        "modifiers": [],
        "childrenConditions": ["debilitado", "lento", "vulneravel"],
        "tooltip": "<strong>Exausto</strong><br><br> O personagem fica debilitado, lento e vulnerável. Se ficar exausto novamente, em vez disso fica inconsciente. <i>Condição de fadiga.</i>",
        "durationType": "cena"
    },
    "fascinado": {
        "tooltip": "<strong>Fascinado</strong><br><br> Com a atenção presa em alguma coisa. O personagem sofre –5 em Percepção e não pode fazer ações, exceto observar aquilo que o fascinou. Qualquer ação hostil contra o personagem anula esta condição. Balançar uma criatura fascinada para tirá-la desse estado gasta uma ação padrão. <i>Condição mental.</i>"
    },
    "fatigado": {
        "tooltip": "<strong>Fatigado</strong><br><br> O personagem fica fraco e vulnerável. Se o personagem ficar fatigado novamente, em vez disso fica exausto. <i>Condição de fadiga.</i>"
    },
    "fraco": {
        "tooltip": "<strong>Fraco</strong><br><br>  O personagem sofre –2 em testes de atributos físicos (Força, Destreza e Constituição) e de perícias baseadas nesses atributos. Se ficar fraco novamente, em vez disso fica debilitado."
    },
    "frustrado": {
        "tooltip": "<strong>Frustrado</strong><br><br> O personagem sofre –2 em testes de atributos mentais (Inteligência, Sabedoria e Carisma) e de perícias baseadas nesses atributos. Se ficar frustrado novamente, em vez disso fica esmorecido. <i>Condição mental.</i>"
    },
    "imovel": {
        "tooltip": "<strong>Imóvel</strong><br><br> Todas as formas de deslocamento do personagem são reduzidas a 0m. <i>Condição de paralisia.</i>"
    },
    "inconsciente": {
        "tooltip": "<strong>Inconsciente</strong><br><br> O personagem fica indefeso e não pode fazer ações. Balançar uma criatura para acordá-la gasta uma ação padrão."
    },
    "indefeso": {
        "tooltip": "<strong>Indefeso</strong><br><br> O personagem é considerado desprevenido, mas sofre –10 na Defesa, falha automaticamente em testes de Reflexos e pode sofrer golpes de misericórdia."
    },
    "lento": {
        "tooltip": "<strong>Lento</strong><br><br> Todas as formas de deslocamento do personagem são reduzidas à metade (arredonde para baixo para o primeiro incremento de 1,5m) e ele não pode correr ou fazer investidas. <i>Condição de paralisia.</i>"
    },
    "morto": {
        "tooltip": "<strong>Morto</strong><br><br> O personagem está morto!"
    },
    "ofuscado": {
        "tooltip": "<strong>Ofuscado</strong><br><br> O personagem sofre –2 em testes de ataque e de Percepção. <i>Condição de sentidos.</i>"
    },
    "paralisado": {
        "tooltip": "<strong>Paralisado</strong><br><br> O personagem fica imóvel e indefeso e só pode realizar ações puramente mentais. <i>Condição de paralisia.</i>"
    },
    "pasmo": {
        "tooltip": "<strong>Pasmo</strong><br><br> O personagem não pode fazer ações, exceto reações. <i>Condição mental.</i>"
    },
    "petrificado": {
        "tooltip": "<strong>Petrificado</strong><br><br> O personagem fica inconsciente e recebe resistência a dano 8."
    },
    "sangrando": {
        "tooltip": "<strong>Sangrando</strong><br><br> Com um ferimento aberto. No início de seus turnos, o personagem deve fazer um teste de Constituição (CD 15). Se passar, estabiliza e remove essa condição. Se falhar, sofre 1d6 pontos de dano e continua sangrando."
    },
    "surdo": {
        "tooltip": "<strong>Surdo</strong><br><br> O personagem não pode fazer testes de Percepção para ouvir e sofre –5 em testes de Iniciativa. Além disso, é considerado em condição ruim para lançar magias. <i>Condição de sentidos.</i>"
    },
    "surpreendido": {
        "tooltip": "<strong>Surpreendido</strong><br><br> Não ciente de seus inimigos. O personagem fica desprevenido e não pode fazer ações, exceto reações."
    },
    "vulneravel": {
        "tooltip": "<strong>Vulnerável</strong><br><br> O personagem sofre –2 na Defesa."
    },
    "sobrecarregado": {
        "tooltip": "<strong>Sobrecarregado</strong><br><br> O personagem sobre -2 de penalidade de armadura e tem seu deslocamento reduzido em 3m."
    }
}

T20Config.atributos = {
	"for": "Força",
	"des": "Destreza",
	"con": "Constituição",
	"int": "Inteligência",
	"sab": "Sabedoria",
	"car": "Carisma"
};

T20Config.atributosAbr = {
	"for": "For",
	"des": "Des",
	"con": "Con",
	"int": "Int",
	"sab": "Sab",
	"car": "Car"
};

/* ------------------ Classes ----------------- */

T20Config.pvPorNivel = [2, 3, 4, 5, 6];
T20Config.pmPorNivel = [3, 4, 5, 6];

T20Config.atributoPV = {
	"forPV": "Força",
	"desPV": "Destreza",
	"intPV": "Inteligência",
	"sabPV": "Sabedoria",
	"carPV": "Carisma"
}
T20Config.atributoPM = {
	"forPM": "Força",
	"desPM": "Destreza",
	"conPM": "Constituição",
	"intPM": "Inteligência",
	"sabPM": "Sabedoria",
	"carPM": "Carisma"
}

/* ------------------ Perícias ---------------- */

T20Config.pericias = {
    "acr": "Acrobacia",
    "ade": "Adestramento",
    "atl": "Atletismo",
    "atu": "Atuação",
    "cav": "Cavalgar",
    "con": "Conhecimento",
    "cur": "Cura",
    "dip": "Diplomacia",
    "eng": "Enganação",
    "for": "Fortitude",
    "fur": "Furtividade",
    "gue": "Guerra",
    "ini": "Iniciativa",
    "int": "Intimidação",
    "intu": "Intuição",
    "inv": "Investigação",
    "jog": "Jogatina",
    "lad": "Ladinagem",
    "lut": "Luta",
    "mis": "Misticismo",
    "nob": "Nobreza",
    "ofi": "Ofício",
    "per": "Percepção",
    "pil": "Pilotagem",
    "pon": "Pontaria",
    "ref": "Reflexos",
    "rel": "Religião",
    "sob": "Sobrevivência",
    "von": "Vontade"
};

T20Config.resistencias = {
	"for": "Fortitude",
	"ref": "Reflexos",
	"von": "Vontade"
}

/* ------------------- Níveis ----------------- */

T20Config.xpPorNivel = [
  0, 1000, 3000, 6000, 10000, 15000, 21000, 28000, 36000, 45000, 55000, 66000,
	78000, 91000, 105000, 120000, 136000, 153000, 171000, 190000, 
];

/* ---------------- Proficiências ------------- */

T20Config.tamanhos = {
	"Minusculo":"Minúsculo",
	"Pequeno":"Pequeno",
	"Medio":"Médio",
	"Grande":"Grande",
	"Enorme":"Enorme",
	"Colossal":"Colossal"
};

T20Config.idiomas = {
	"comum": "Comum",
	"anao": "Anão",
	"elfico": "Élfico",
	"goblin": "Goblin",
	"hynne": "Hynne",
	"silvestre": "Silvestre",
	"taurico": "Táurico",
	"abissal": "Abissal",
	"aquan": "Aquan",
	"auran": "Auran",
	"celestial": "Celestial",
	"draconico": "Dracônico",
	"gigante": "Gigante",
	"gnoll": "Gnoll",
	"ignan": "Ignan",
	"infernal": "Infernal",
	"orc": "Orc",
	"terran": "Terran"
};

T20Config.profArmas = {
  "sim": "Armas Simples",
  "mar": "Armas Marciais",
  "exo": "Armas Exóticas",
  "fog": "Armas de Fogo"
};

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
T20Config.profArmaduras = {
  "leve": "Armaduras Leves",
  "pesada": "Armaduras Pesadas",
  "escudo": "Escudos"
};

T20Config.skillCompendiumEntries = {
	'acr': 'tormenta20.pericias.hWto9ixrLcAV5Pg9',
	'ade': 'tormenta20.pericias.3vNJgVz4DeSWUr2T',
	'atl': 'tormenta20.pericias.ZMXVeEARJHe4WsWx',
	'atu': 'tormenta20.pericias.eiKvnk0prKaG2rl0',
	'cav': 'tormenta20.pericias.umGf2m2a517affYh',
	'con': 'tormenta20.pericias.k4BBdUTIFCFMi6Tp',
	'cur': 'tormenta20.pericias.f0Cvg9dhaAgVfjpU',
	'dip': 'tormenta20.pericias.u3bLZxqLhvqQjlfz',
	'eng': 'tormenta20.pericias.IAzI6D4Xj2vZa4ZK',
	'for': 'tormenta20.pericias.efy0eGXTMnXZbXED',
	'fur': 'tormenta20.pericias.u2FeyQUF5rSu9ec3',
	'gue': 'tormenta20.pericias.7pA3UiVhl2H091zb',
	'ini': 'tormenta20.pericias.W4V1ObkLtpxgRoBE',
	'int': 'tormenta20.pericias.tGOSDh0RSh0Qewdv',
	'intu': 'tormenta20.pericias.DLcZCk8c9KUyJ0Qk',
	'inv': 'tormenta20.pericias.ZAS0zJaPRJWg15ib',
	'jog': 'tormenta20.pericias.htoLZR83iKDkmc8j',
	'lad': 'tormenta20.pericias.lcgrJelGuEGZ96C0',
	'lut': 'tormenta20.pericias.6PwE0u0Nfr5VolzM',
	'mis': 'tormenta20.pericias.oPAtmZ2vXGDjfCyt',
	'nob': 'tormenta20.pericias.Uw4Edff5CDnPZgIF',
	'ofi': 'tormenta20.pericias.iviKBc19dNgZYBJ0',
	'per': 'tormenta20.pericias.hkTML0lqoOopGTLQ',
	'pil': 'tormenta20.pericias.d7ia26SZqoFGzGrv',
	'pon': 'tormenta20.pericias.rwDmhFebCtNIFzfy',
	'ref': 'tormenta20.pericias.p9JngLpUrPFrqiTU',
	'rel': 'tormenta20.pericias.VYEkUN1ITAQqn5y7',
	'sob': 'tormenta20.pericias.dyePg3NS95So0yEJ',
	'von': 'tormenta20.pericias.mBbMbDy7XnbkFzdR'
}

/*------------------- Itens -------------------*/

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
T20Config.listaDuracoes = {
	"instant": "Instantânea",
	"cena": "Cena",
  "turno": "Turno",
  "rodada": "Rodada",
	"sust": "Sustentada",
	"verTexto": "Ver Texto",
	"outra": "Outra"
};

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
T20Config.listaAtivacao = {
  "padrao": "Padrão",
  "movimento": "Movimento",
	"completa": "Completa",
  "reacao": "Reação",
  "livre": "Livre"
};

/*------------------- Armas -------------------*/

/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
T20Config.weaponTypes = {
  "simples": "Simples",
  "marcial": "Marcial",
  "exotica": "Exótica",
  "armaDeFogo": "Arma de Fogo",
  "natural": "Natural",
  "improvisada": "Improvisada"
};

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
T20Config.weaponProperties = {
  "adaptavel": "Adaptável",
  "agil": "Ágil",
  "alongada": "Alongada",
  "arremesso": "Arremesso",
  "ataqueDistancia": "Ataque à Distância",
  "duasMaos": "Duas Mãos",
  "dupla": "Dupla",
  "leve": "Leve",
  "municao": "Munição",
  "versatil": "Versátil"
};

T20Config.passosDano = {
    "1d3": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "1d4": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "1d6": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "1d8": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "2d4": ["1", "1d2", "1d3", "1d4", "1d6", "2d4", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "1d10": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "1d12": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "2d6": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "2d6", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "3d4": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "3d4", "3d6", "4d6", "4d8", "4d10", "4d12"],
    "2d8": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "2d6", "2d8", "3d8", "4d8", "4d10", "4d12"],
    "2d10": ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "2d6", "2d8", "2d10", "3d10", "4d10", "4d12"],
    arr1: ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "1d12", "3d6", "4d6", "4d8", "4d10", "4d12"],
    arr2: ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "2d6", "2d8", "3d8", "4d8", "4d10", "4d12"],
    arr3: ["1", "1d2", "1d3", "1d4", "1d6", "1d8", "1d10", "2d6", "2d8", "2d10", "3d10", "4d10", "4d12"]
}

T20Config.encantosArmas = { //Não Implementado
  "arremesso": {
    "id": "arremesso",
    "name": "Arremesso",
    "tooltip": "A arma pode ser arremessada em alcance curto. Caso já pudesse ser arremessada, seu alcance aumenta em uma categoria. No fim do seu turno, a arma volta voando para você. Pegar a arma é uma ação livre."
  },
  "lancinante": {
      "id": "lancinante",
      "name": "Lancinante",
      "tooltip": "A arma inflige ferimentos mortais. Quando faz um acerto crítico com a arma, além de multiplicar os dados de dano, você também multiplica quaisquer bônus numéricos."
  }
};
